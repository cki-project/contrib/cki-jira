Name: kwf-lib
# From __version__
Version: 1.0.0
Release: %autorelease
Summary: A collection of classes, functions and scripts related to kernel workflow.

License: GPL-3.0-or-later
URL: https://gitlab.com/cki-project/kwf-lib

Source0: https://gitlab.com/cki-project/contrib/kwf-lib/-/archive/v%{version}/kwf-lib-v%{version}.tar.gz

BuildArch: noarch
BuildRequires: python3-devel

%description
A Python library that contains classes, functions and scripts to deal with
Jira issues, Bugzillas, CVEs and other important stuff related to kernel
workflow.

%prep
%autosetup -n kwf-lib-v%{version}

%generate_buildrequires
%pyproject_buildrequires -e spec

%build
%pyproject_wheel

%install
%pyproject_install
%pyproject_save_files kwf_lib

%check
%tox

%files  -f %{pyproject_files}
%license LICENSE
%doc README.md LICENSE

%changelog
%autochangelog
