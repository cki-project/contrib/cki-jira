# kwf-lib

Common place to put all scripts related to kernel workflow.

## Design

This repository holds all the common python code that is defined by the kernel workflow processes.

This library is for you, if you are looking for any component or facility to deal with:

- RHEL Jira issues and Bugzillas (if applicable) unified and easily.
- CVEs.
- Z-Stream and Y-Stream development.

This package can be found on Copr as [@cki/kwf-lib](https://copr.fedorainfracloud.org/coprs/g/cki/kwf-lib)

## Developer guidelines

**Use care when making changes to this repository.** Many other applications
depend on this code to work properly.

### Install

If you want to get started with development, you can install the
dependencies by running:

```shell
pip install --user .[dev]
```

_This library does not have any third-party requirement._

### Running tests and linting

Install some basic dependencies first.

```shell
sudo dnf install jq toilet lolcat direnv
```

Activate the `venv` and then install all Python dependencies.

```shell
pip install tox -e .[dev]
```

To run linting and tests, use `tox`.

```shell
podman run --pull=newer --rm -it --volume .:/code:Z --workdir /code quay.io/cki/cki-tools:production tox -e py312
```

**Specify and install the proper Python interpreter, because `mypy` fails with
Python 3.13 yet.**
