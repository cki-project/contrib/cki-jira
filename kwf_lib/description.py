"""A module for representing KWF tag values found in text."""
from functools import lru_cache
import re
import typing

from typing_extensions import Self

from kwf_lib.common import BUGZILLA_ID_PATTERN
from kwf_lib.common import BUGZILLA_ID_REGEX
from kwf_lib.common import BugzillaID
from kwf_lib.common import CVE_ID_REGEX
from kwf_lib.common import CveID
from kwf_lib.common import JIRA_KEY_PATTERN
from kwf_lib.common import JiraKey

# Regex pattern building blocks.
BASE_URL_PATTERN = r'(?P<scheme>https?)://(?P<hostname>[^/\s]+)'
NUMERIC_PATTERN = r'(?P<id>\d+)'
SHA_SHORT_PATTERN = r'(?P<id>[0-9a-fA-F]{12,40})'
SHA_FULL_PATTERN = r'(?P<id>[0-9a-fA-F]{40})'

# Tag re.Patterns. Make sure these all begin with ^ and end with $.
BUGZILLA_URL_REGEX = \
    re.compile(rf'^{BASE_URL_PATTERN}/(?:show_bug\.cgi\?id=)?{BUGZILLA_ID_PATTERN}$')
CC_REGEX = re.compile(r'^(?:(?P<name>.+) )?<(?P<email>.+)>$')
DEPENDS_ID_REGEX = re.compile(rf'^!{NUMERIC_PATTERN}$')
GITLAB_MR_REGEX = \
    re.compile(rf'^{BASE_URL_PATTERN}/(?P<namespace>.*)/-/merge_requests/{NUMERIC_PATTERN}$')
JIRA_URL_REGEX = re.compile(rf'^{BASE_URL_PATTERN}/browse/(?P<id>{JIRA_KEY_PATTERN})$')
NUMERIC_REGEX = re.compile(rf'^{NUMERIC_PATTERN}$')
OMITTED_FIX_REGEX = re.compile(rf'^{SHA_SHORT_PATTERN} (?P<title>.*)$')
SHA_FULL_REGEX = re.compile(rf'^{SHA_FULL_PATTERN}$')
SHA_SHORT_REGEX = re.compile(rf'^{SHA_SHORT_PATTERN}$')
SIGNOFF_REGEX = re.compile(r'^(?P<name>.+) <(?P<email>.+)>$')
STRING_REGEX = re.compile(r'^(?P<id>.*)$')

# "RHEL Only" pattern for `Upstream status:` tag, used to set `Description.rhel_only` bool.
RHEL_ONLY_REGEX = re.compile('^RHEL[ -]*[0-9.Zz]*[ -][Oo]nly$')


class Tag(typing.NamedTuple):
    """Tag parser."""

    prefixes: tuple[str, ...]
    patterns: tuple[re.Pattern[str], ...]
    type: typing.Callable[[str], typing.Any]

    def lines(self, text: str) -> list[str]:
        """Return list of lines which match a prefix, with prefix & trailing whitespace removed."""
        prefixes = [f'{prefix}: ' for prefix in self.prefixes]

        return [line.removeprefix(prefix).rstrip() for
                line in text.splitlines() for prefix in prefixes if line.startswith(prefix)]

    @lru_cache(maxsize=32)
    def matches(self, text: str, *, strict: bool = False, **kwargs: str) -> list[re.Match[str]]:
        """Return the list of re.Match objects matched from the input text lines."""
        matches: list[re.Match[str]] = []

        for line in self.lines(text):
            for pattern in self.patterns:
                # If strict matching is enabled and the Pattern does not have all the group names
                # given via the kwargs then do not consider it.
                if strict and kwargs.keys() - pattern.groupindex.keys():
                    continue

                if match := pattern.match(line):
                    matches.append(match)
                    break

        return [match for match in matches if
                all(v == match.group(k) for k, v in kwargs.items() if k in match.re.groupindex)]

    def match(self, text: str, **kwargs: str) -> set[typing.Any]:
        """Return the set of matching values for the given text."""
        return {self.type(match.group('id') if 'id' in match.re.groupindex else  # type: ignore
                          match.groups()) for match in self.matches(text, **kwargs)}


# Tags described in the CommitRules.
# https://redhat.gitlab.io/centos-stream/src/kernel/documentation/docs/commitrules.html
COMMIT_RULES_TAGS = [
    Tag(('Cc', 'CC'), patterns=(CC_REGEX,), type=tuple),
    Tag(('CVE',), patterns=(CVE_ID_REGEX,), type=CveID),
    Tag(('Depends',), patterns=(GITLAB_MR_REGEX, DEPENDS_ID_REGEX), type=int),
    Tag(('JIRA',), patterns=(JIRA_URL_REGEX,), type=JiraKey),
    Tag(('Omitted-fix',), patterns=(OMITTED_FIX_REGEX,), type=str),
    Tag(
        ('Upstream Status', 'Upstream status', 'Upstream-Status', 'Upstream-status'),
        patterns=(STRING_REGEX,), type=str
    ),
    Tag(('Signed-off-by',), patterns=(SIGNOFF_REGEX,), type=tuple),
]

# Tags that have been retired from CommitRules.
RETIRED_COMMIT_RULES_TAGS = [
    Tag(('Bugzilla',), patterns=(BUGZILLA_ID_REGEX, BUGZILLA_URL_REGEX), type=BugzillaID),
    Tag(('Patchwork-id',), patterns=(NUMERIC_REGEX,), type=int),
]

# Additional tags used in the kernel workflow.
KWF_TAGS = [
    Tag(('Y-Bugzilla',), patterns=(BUGZILLA_ID_REGEX, BUGZILLA_URL_REGEX), type=BugzillaID),
    Tag(('Z-Bugzilla',), patterns=(BUGZILLA_ID_REGEX, BUGZILLA_URL_REGEX), type=BugzillaID),
    Tag(('Ignore-duplicate',), patterns=(SHA_SHORT_REGEX,), type=str),
    Tag(('O-JIRA',), patterns=(JIRA_URL_REGEX,), type=JiraKey),
    Tag(('Y-JIRA',), patterns=(JIRA_URL_REGEX,), type=JiraKey),
    Tag(('Z-JIRA',), patterns=(JIRA_URL_REGEX,), type=JiraKey),
    Tag(('MR',), patterns=(GITLAB_MR_REGEX,), type=int),
    Tag(('Y-Commit',), patterns=(SHA_FULL_REGEX,), type=str),
]

_TAGS = COMMIT_RULES_TAGS + RETIRED_COMMIT_RULES_TAGS + KWF_TAGS


class BaseDescription(str):
    """Shared Description attributes and methods."""

    TAGS = {tag.prefixes[0].lower().replace('-', '_').replace(' ', '_'): tag for tag in _TAGS}

    bugzilla: set[BugzillaID]
    y_bugzilla: set[BugzillaID]
    z_bugzilla: set[BugzillaID]
    cc: set[tuple[str | None, str]]
    cve: set[CveID]
    depends: set[int]
    ignore_duplicate: set[str]
    jira: set[JiraKey]
    o_jira: set[JiraKey]
    y_jira: set[JiraKey]
    z_jira: set[JiraKey]
    mr: set[int]
    omitted_fix: set[str]
    patchwork_id: set[int]
    signed_off_by: set[tuple[str, str]]
    upstream_status: set[str]
    y_commit: set[str]

    def __new__(cls, text: str = '') -> Self:
        """If the input is some null value then replace it with an empty string."""
        return super().__new__(cls, text or '')

    @staticmethod
    def _set_tag_attrs_(instance: 'BaseDescription', text: str, tags: dict[str, Tag]) -> None:
        """Set attribute values for each Tag."""
        for tag_name, tag in tags.items():
            setattr(instance, tag_name, tag.match(text))


class Description(BaseDescription):
    # pylint: disable=too-many-instance-attributes
    """Simple string wrapper to expose Tag values."""

    def __init__(self, _: str = '') -> None:
        """Set up attributes for each tag and set the 'internal' & 'rhel_only' attributes."""
        self._set_tag_attrs_(self, self, self.TAGS)

        self.internal = 'INTERNAL' in self.lines('jira')
        self.rhel_only = any(RHEL_ONLY_REGEX.match(line) for line in self.upstream_status)

    def lines(self, tag_name: str) -> list[str]:
        """Return all the tag line strings, regardless of whether they matched any regex."""
        if not (tag := self.TAGS.get(tag_name)):
            raise KeyError(f"Not a valid tag name: '{tag_name}'")

        return tag.lines(self)

    def match(self, tag_name: str, **kwargs: str) -> set[typing.Any]:
        """Return the derived values for the tag, filtered on the kwarg values."""
        if not (tag := self.TAGS.get(tag_name)):
            raise KeyError(f"Not a valid tag name: '{tag_name}'")

        return tag.match(self, **kwargs)


# For humans, these are ^^^NOTES-START^^^ and ^^^NOTES-END^^^.
NOTES_START = r'\^\^\^NOTES-START\^\^\^'
NOTES_END = r'\^\^\^NOTES-END\^\^\^'

NOTES_DESCRIPTION_REGEX = re.compile(
    rf'^(?:(?P<header>.*){NOTES_START})?(?:(?P<notes>.*){NOTES_END})?(?P<commit>.*)$',
    re.DOTALL
)


class NotesDescription(BaseDescription):
    """Parser for a kmt-style text string which may include header fields & git notes values."""

    def __init__(self, _: str) -> None:
        """Parse the text into header, notes, & commit and fix up the attribute values."""
        self.header = ''
        self.notes = Description('')
        self.commit = Description(self)

        # This regex should match any string :shrug:.
        if match := NOTES_DESCRIPTION_REGEX.match(self):
            self.header = match.groupdict().get('header') or ''
            self.notes = Description(match.groupdict().get('notes') or '')
            self.commit = Description(match.groupdict().get('commit') or '')

        # Fudge the notes tag values if they have Z-Bugzilla or Z-JIRA tags.
        # The goal is to replicate the behaviour of kmt.tags.CommitTags.convert_to_y_tags.
        if self.notes.z_bugzilla or self.notes.z_jira:
            self.notes.y_bugzilla = self.notes.bugzilla.copy()
            self.notes.y_jira = self.notes.jira.copy()

            self.notes.bugzilla = self.notes.z_bugzilla.copy()
            self.notes.jira = self.notes.z_jira.copy()

            self.notes.z_bugzilla = set()
            self.notes.z_jira = set()

        # Begin with the tag values from the commit section.
        self._set_tag_attrs_(self, self.commit, self.TAGS)

        # Use the non-empty notes tag values to replace those from the commits.
        # The goal is to replicate the behaviour of kmt.tags.CommitTags.override_by.
        for tag_name in self.TAGS:
            if notes_value := getattr(self.notes, tag_name):
                setattr(self, tag_name, notes_value)

        # The rest of this is attempting to replicate the behavior of kmt.tags.get_msg_tags.
        # If Y-JIRA tag is empty then use O-JIRA values for it.
        if not self.y_jira:
            self.y_jira = self.o_jira

        # If there are Y-Bugzilla or Y-JIRA values then replace Bugzilla & JIRA tag values with
        # their Z- variants.
        if self.y_bugzilla:
            self.z_bugzilla = self.bugzilla
        if self.y_jira:
            self.z_jira = self.jira
