"""Basic definitions."""
from kwf_lib.common import TrackerType

# Typical bugzilla api rest/bug/ payloads do not have any sort of URL or host identifier?
BUGZILLA_HOST = 'bugzilla.redhat.com'

# Default Gitlab hostname.
GITLAB_HOST = 'gitlab.com'

# Components which are compatible with the KWF.
KWF_COMPONENTS = ('kernel', 'kernel-rt', 'kernel-automotive')

# Kernel workflow projects in jira.
KWF_PROJECTS = ('RHEL',)

# TrackerTypes that are compatible with the KWF.
KWF_TRACKER_TYPES = (
    TrackerType.TASK,
    TrackerType.BUG,
    TrackerType.STORY,
    TrackerType.VULNERABILITY,
    TrackerType.WEAKNESS
)


# A jira JQL snippet which matches kernel workflow issues.

def _kwf_jql_filter(
    projects: tuple[str, ...] = KWF_PROJECTS,
    components: tuple[str, ...] = KWF_COMPONENTS,
    tracker_types: tuple[TrackerType, ...] = KWF_TRACKER_TYPES
) -> str:
    """Return a JQL string to filter out anything that is obviously not a KWF related issue."""
    projects_str = ', '.join(projects)
    components_str = '|'.join(components)
    # common.TrackerType member values are a tuple with the first item being the name.
    issue_types_str = ', '.join(issue_type.value[0] for issue_type in tracker_types)

    kwf_jql = []

    kwf_jql.append(f"project in ({projects_str})")
    kwf_jql.append(f"issuetype in ({issue_types_str})")
    kwf_jql.append(f'component in componentMatch("^({components_str})($| / .*$)")')

    return ' AND '.join(kwf_jql)


KWF_JQL_FILTER = _kwf_jql_filter()
